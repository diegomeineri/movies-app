
## Welcome to Movies app! 
### Getting Started
To initialize the project, install the packages with the following command:

    yarn install
    
Run the project with the following command:

    yarn start

## Project Information:

       Ionic CLI: 6.20.8 
       Ionic Framework : @ionic/angular 6.5.4
       @angular-devkit/build-angular: 15.1.6
       @angular-devkit/schematics: 14.2.10
       @angular/cli: 14.2.10
       @ionic/angular-toolkit: 6.1.0

## Features

- Login and register.
- Change app Logo.
- See all movies.
- Edit Movie.
- Delete movie.
- Search Movie.
- Rate movie.

