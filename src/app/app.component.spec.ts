import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { By } from '@angular/platform-browser';

import { RouterTestingModule } from '@angular/router/testing';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { BehaviorSubject } from 'rxjs';

import { AppComponent } from './app.component';
import { MoviesDataService } from './modules/movies/shared-movies/services/movies-data/movies-data.service';
import { UsersService } from './modules/users/shared-users/services/users/users.service';
import { FakeNavController } from './testing/fakes/nav-controller.fake.spec';

describe('AppComponent', () => {
  let moviesDataServiceSpy: jasmine.SpyObj<MoviesDataService>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;
  let storageServiceSpy: jasmine.SpyObj<Storage>;
  let usersServiceSpy: jasmine.SpyObj<UsersService>;
  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
        set: (_d: any) => new Promise((resolve, _reject) => resolve('ok')),
      }),
    }),
  };

  beforeEach(async () => {
    usersServiceSpy = jasmine.createSpyObj('UsersService', {
      login: Promise.resolve(),
    });
    storageServiceSpy =  jasmine.createSpyObj('Storage', {
      set:Promise.resolve()
    });
    moviesDataServiceSpy = jasmine.createSpyObj('MoviesDataService', {
      init:Promise.resolve()
    });
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule.withRoutes([])],
      providers: [
        { provide: AngularFireAuth, useValue: FirestoreStub },
        { provide: MoviesDataService, useValue: moviesDataServiceSpy },
        { provide: NavController, useValue: navControllerSpy },
        { provide: Storage, useValue: storageServiceSpy },
        { provide: UsersService, useValue: usersServiceSpy },
      ],
    }).compileComponents();
  });

  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should have urls', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const [menuItem1, menuItem2] = fixture.debugElement.queryAll(By.css('ion-item.item'));
    expect(menuItem1).toBeTruthy()
    expect(menuItem2).toBeTruthy()
  });
});
