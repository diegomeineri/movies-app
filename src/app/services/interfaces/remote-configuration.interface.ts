export interface RemoteConfiguration {
  initialize(): Promise<void>;
  getObject(name: string): any;
}
