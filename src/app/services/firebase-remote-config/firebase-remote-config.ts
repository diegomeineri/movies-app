import { FirebaseApp } from 'firebase/app';
import * as firebase from 'firebase/remote-config';
import { RemoteConfig } from 'firebase/remote-config';
import { RemoteConfiguration } from '../interfaces/remote-configuration.interface';


export class FirebaseRemoteConfig implements RemoteConfiguration {
  firebaseRemoteConfig = firebase;
  private remoteConfig: RemoteConfig;
  private fetchTimeMillis = 2000;

  constructor(private readonly firebaseApp: FirebaseApp) {}

  initialize(): Promise<void> {
    this.remoteConfig = this.firebaseRemoteConfig.getRemoteConfig(this.firebaseApp);
    this.setDefaultConfig();
    return this.fetchAndActivate();
  }

  fetchAndActivate(): Promise<void> {
    return this.firebaseRemoteConfig.fetchAndActivate(this.remoteConfig) as Promise<any>;
  }

  setDefaultConfig() {
    this.remoteConfig.settings.minimumFetchIntervalMillis = this.fetchTimeMillis;
  }

  getObject(name: string): any {
    return JSON.parse(this.firebaseRemoteConfig.getValue(this.remoteConfig, name).asString());
  }
}
