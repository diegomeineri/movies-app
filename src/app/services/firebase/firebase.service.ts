import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  importedFirebase = firebase;
  constructor() {}

  async init(): Promise<void> {
    this.importedFirebase.initializeApp(environment.firebase);
  }

  getApp(): firebase.FirebaseApp {
    return this.importedFirebase.getApp();
  }
}
