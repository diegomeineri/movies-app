import { TestBed } from '@angular/core/testing';
import { FirebaseService } from './firebase.service';

describe('FirebaseService', () => {
  let service: FirebaseService;
  let firebaseSpy: any;
  beforeEach(() => {
    firebaseSpy = jasmine.createSpyObj(
      'FirebaseNamespace',
      { initializeApp: null, getApp: {}},
    );
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseService);
    service.importedFirebase = firebaseSpy;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get initialized app on getApp', () => {
    service.getApp();
    expect(firebaseSpy.getApp).toHaveBeenCalledTimes(1);
  });
});
