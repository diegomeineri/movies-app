
import { firebaseInitializer } from './firebase-initializer';
import { fakeAsync, tick } from '@angular/core/testing';
import { RemoteConfigService } from '../remote-config/remote-config.service';
import { FirebaseService } from '../firebase/firebase.service';

describe('firebaseInitializer', () => {
  let remoteConfigSpy: jasmine.SpyObj<RemoteConfigService>;
  let firebaseSpy: jasmine.SpyObj<FirebaseService>;

  beforeEach(() => {
    remoteConfigSpy = jasmine.createSpyObj('RemoteConfigService', { initialize: Promise.resolve() });
    firebaseSpy = jasmine.createSpyObj('FirebaseService', { init: Promise.resolve(), getApp: null });
  });

  it('should initialize services', fakeAsync(() => {
    firebaseInitializer(remoteConfigSpy, firebaseSpy)();
    tick();
    expect(remoteConfigSpy.initialize).toHaveBeenCalledTimes(1);
    expect(firebaseSpy.init).toHaveBeenCalledTimes(1);
    expect(firebaseSpy.getApp).toHaveBeenCalledTimes(1);
  }));
  
});
