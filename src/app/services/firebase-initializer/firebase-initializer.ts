import { FirebaseRemoteConfig } from "../firebase-remote-config/firebase-remote-config";
import { FirebaseService } from "../firebase/firebase.service";
import { RemoteConfigService } from "../remote-config/remote-config.service";

export function firebaseInitializer(remoteConfig: RemoteConfigService, firebase: FirebaseService) {
  return () => firebase.init().then(() => remoteConfig.initialize(new FirebaseRemoteConfig(firebase.getApp()))).catch(()=>{});
}
