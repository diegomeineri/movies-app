import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { MovieDetailPage } from './movie-detail.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared-movies/shared-movies.module';

const routes: Routes = [
  {
    path: '',
    component: MovieDetailPage,
  },
  {
    path: ':id',
    component: MovieDetailPage,
  },
];

@NgModule({
  imports: [
    SharedMoviesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [MovieDetailPage]
})
export class MovieDetailPageModule {}
