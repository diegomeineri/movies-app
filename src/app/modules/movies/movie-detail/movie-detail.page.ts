import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { TwoButtonsAlertComponent } from '../shared-movies/components/two-buttons-alert/two-buttons-alert.component';
import { Movie } from '../shared-movies/interfaces/movie.interface';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';

@Component({
  selector: 'app-movie-detail',
  template: `
    <ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=''
            mode="ios"
            defaultHref="/movies/home"
          ></ion-back-button>
        </ion-buttons>
        <ion-buttons class="md__header__end-icons" slot="end">
        <ion-icon class="delete" (click)="this.showDeleteAlert()" name="trash-sharp"></ion-icon>
        <ion-icon class="edit" (click)="this.goToEdit()" name="pencil-sharp"></ion-icon>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content  class="md">
      <div class="md__body">
        <ion-text class="md__body__title">{{this.movie.title}}</ion-text>
        <div class="md__body__image">
          <img [src]="this.movie.img" />
        </div>
        <div class="md__body__rate-and-premiere">
          <div class="md__body__rate-and-premiere__rate">
            <ion-text> Rate: </ion-text>
            <ion-rating-stars
              [color]="'white'"
              [rating]="this.movie.rate"
              [filledColor]="'yellow'"
              [margin]="2"
              [size]="20"
              [disabled]="true"
            ></ion-rating-stars>
          </div>
          <div class="md__body__rate-and-premiere__premiere">
            <ion-text>{{this.movie.premiere}}</ion-text>
          </div>
        </div>
        <div class="md__body__description">
          <ion-text>{{this.movie.description}}</ion-text>
        </div>
      </div>
    </ion-content>
  `,
  styleUrls: ['./movie-detail.page.scss'],
})
export class MovieDetailPage implements OnInit {
  constructor(private navController: NavController, private route: ActivatedRoute, private modalController : ModalController, private moviesDataService: MoviesDataService) {}
  movie: Movie;
  id : number;

  ngOnInit() {
    this.movieId();
    this.getMovie();
  }
  
  getMovie(){
    this.movie = this.moviesDataService.getMovie(this.id)[0];
  }

  movieId() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string);
  }

  async showDeleteAlert() {
    const modal = await this.modalController.create({
      component: TwoButtonsAlertComponent,
      cssClass: 'modal',
      backdropDismiss: false,
      componentProps: {
        title: 'Delete movie',
        description: 'Are you sure you want to delete the movie?',
        confirmButton: 'Confirm',
        cancelButton: 'Cancel',
      },
    });

    await modal.present();
    const { role } = await modal.onDidDismiss();
    if (role === 'confirm') {
      await this.deleteMovie();
    }
  }

  deleteMovie(){
    this.moviesDataService.deleteMovie(this.id);
    this.navController.navigateRoot('/movies/home');
  }

  goToEdit(){
    this.navController.navigateForward(['movies/editor/', this.id])
  }
}
