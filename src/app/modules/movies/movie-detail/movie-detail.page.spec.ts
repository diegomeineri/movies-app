import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { IonicModule, ModalController, NavController } from '@ionic/angular';
import { FakeActivatedRoute } from 'src/app/testing/fakes/activated-route.fake.spec';
import { FakeModalController } from 'src/app/testing/fakes/modal-controller.fake.spec';
import { FakeNavController } from 'src/app/testing/fakes/nav-controller.fake.spec';
import { Movie } from '../shared-movies/interfaces/movie.interface';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';

import { MovieDetailPage } from './movie-detail.page';

describe('MovieDetailPage', () => {
  let component: MovieDetailPage;
  let fixture: ComponentFixture<MovieDetailPage>;
  let fakeActivatedRoute: FakeActivatedRoute;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;
  let moviesDataServiceSpy: jasmine.SpyObj<MoviesDataService>;
  let fakeModalController: FakeModalController;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;

  const movie = {
    id: 1,
    title: 'Batman',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    rate: 1,
    img: 'assets/img/movies/batman.jpg',
    premiere: '10/03/2021',
  };

  beforeEach(waitForAsync(() => {
    moviesDataServiceSpy = jasmine.createSpyObj('MoviesDataService', {
      getMovie: [movie],
    });
    fakeModalController = new FakeModalController(undefined, {});
    modalControllerSpy = fakeModalController.createSpy();
    fakeActivatedRoute = new FakeActivatedRoute({ id: '1' });
    activatedRouteSpy = fakeActivatedRoute.createSpy();
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    TestBed.configureTestingModule({
      declarations: [MovieDetailPage],
      imports: [IonicModule.forRoot()],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteSpy },
        { provide: MoviesDataService, useValue: moviesDataServiceSpy },
        { provide: ModalController, useValue: modalControllerSpy },
        { provide: NavController, useValue: navControllerSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show delete alert when delete button is clicked', () => {
    const deleteButtonEl = fixture.debugElement.query(By.css('ion-icon.delete'));
    deleteButtonEl.nativeElement.click();
    fixture.detectChanges();
    expect(modalControllerSpy.create).toHaveBeenCalledTimes(1);
  });

  it('should navigate to edit page when edit button is clicked', () => {
    const editButtonEl = fixture.debugElement.query(By.css('ion-icon.edit'));
    editButtonEl.nativeElement.click();
    fixture.detectChanges();
    expect(navControllerSpy.navigateForward).toHaveBeenCalledOnceWith(['movies/editor/', movie.id]);
  });

  it('should render properly', () => {
    const movieImageEl = fixture.debugElement.query(By.css('div.md__body__image img'));
    const movieTitleEl = fixture.debugElement.query(By.css('ion-text.md__body__title'));
    const movieRateEl = fixture.debugElement.query(By.css('ion-rating-stars'));
    const movieDescriptionEl = fixture.debugElement.query(By.css('div.md__body__description ion-text'));
    const moviePremiereEl = fixture.debugElement.query(By.css('div.md__body__rate-and-premiere__premiere ion-text'));

    expect(movieImageEl).toBeTruthy();
    expect(movieRateEl).toBeTruthy();
    expect(movieDescriptionEl).toBeTruthy();
    expect(moviePremiereEl).toBeTruthy();
    expect(movieTitleEl).toBeTruthy();
  });
});
