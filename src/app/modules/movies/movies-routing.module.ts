import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from 'src/app/guards/auth/auth.guard';

const routes: Routes = [
  {
    path: 'movies',
    canActivate:[AuthGuard],
    children: [
      {
        path: 'home',
        loadChildren: () => import('./movies-home/movies-home.module').then((m) => m.MoviesHomePageModule),
      },
      {
        path: 'detail',
        loadChildren: () => import('./movie-detail/movie-detail.module').then( m => m.MovieDetailPageModule)
      },
      {
        path: 'editor',
        loadChildren: () => import('./movie-editor/movie-editor.module').then( m => m.MovieEditorPageModule)
      },
    ],
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MoviesRoutingModule {}
