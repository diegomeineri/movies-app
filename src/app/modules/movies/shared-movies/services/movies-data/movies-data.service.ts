import { Injectable } from '@angular/core';
import { RemoteConfigService } from 'src/app/services/remote-config/remote-config.service';
import { Movie } from '../../interfaces/movie.interface';
@Injectable({
  providedIn: 'root',
})
export class MoviesDataService {
  movies : Movie[];
  constructor(private remoteConfig: RemoteConfigService) {}

  init() {
    this.movies = this.remoteConfig.getObject('movies');
  }

  getMovies() {
    return this.movies;
  }

  indexOf(_aMovie: Movie) {
    return this.movies.indexOf(_aMovie);
  }

  getMovie(id: number) {
    const movie = this.getMovies().filter((movie: Movie) => movie.id === id);
    return movie;
  }

  editMovie(index: number, movie: Movie) {
    this.movies[index] = movie;
  }

  deleteMovie(id: number) {
    this.movies = this.movies.filter((movie: Movie) => movie.id !== id);
  }
}
