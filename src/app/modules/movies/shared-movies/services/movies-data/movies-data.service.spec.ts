import { MoviesDataService} from './movies-data.service';
import { TestBed } from '@angular/core/testing';


describe('MoviesDataService', () => {
  let service: MoviesDataService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MoviesDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

});
