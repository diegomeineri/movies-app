import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule, ModalController } from '@ionic/angular';
import { FakeModalController } from 'src/app/testing/fakes/modal-controller.fake.spec';

import { UxDatetimeComponent } from './ux-datetime.component';

describe('UxDatetimeComponent', () => {
  let component: UxDatetimeComponent;
  let fixture: ComponentFixture<UxDatetimeComponent>;
  let fakeModalController: FakeModalController;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  const event = {
    detail: {
      value: '2023-02-23T15:22:00-03:00',
    },
  };
  beforeEach(waitForAsync(() => {
    fakeModalController = new FakeModalController(undefined, {});
    modalControllerSpy = fakeModalController.createSpy();
    TestBed.configureTestingModule({
      declarations: [UxDatetimeComponent],
      imports: [IonicModule.forRoot()],
      providers: [{ provide: ModalController, useValue: modalControllerSpy }],
    }).compileComponents();

    fixture = TestBed.createComponent(UxDatetimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dismiss modal when select date', () => {
    fixture.debugElement
      .query(By.css('ion-datetime'))
      .triggerEventHandler('ionChange', event);
    expect(modalControllerSpy.dismiss).toHaveBeenCalledOnceWith('23/02/2023');
  });
});
