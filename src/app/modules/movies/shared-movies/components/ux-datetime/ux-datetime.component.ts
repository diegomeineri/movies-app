import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { format, parseISO } from 'date-fns';

@Component({
  selector: 'app-ux-datetime',
  template: `
    <ion-datetime
      color="dark"
      presentation="date"
      [showDefaultButtons]="true"
      (ionChange)="this.selectDate($event)"
    >
    </ion-datetime>
  `,
  styleUrls: ['./ux-datetime.component.scss'],
})
export class UxDatetimeComponent implements OnInit {
  constructor(private modalController: ModalController) {}

  ngOnInit() {}

  selectDate(event: any) {
    const formattedDate = format(parseISO(event.detail.value), 'd/MM/yyyy');
    this.modalController.dismiss(formattedDate);
  }
}
