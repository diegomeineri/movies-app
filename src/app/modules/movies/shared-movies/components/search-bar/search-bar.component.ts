import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  template: `
    <ion-searchbar
      color="dark"
      class="search-bar"
      [placeholder]="'Find Movie'"
      clear-icon="ux-clear-button"
      (ionChange)="handleChange($event)"
    ></ion-searchbar>
  `,
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  @Output() search: EventEmitter<string> = new EventEmitter<string>();
  
  constructor() {}

  ngOnInit() {}

  handleChange(event: any) {
    this.search.emit(event);
  }
}
