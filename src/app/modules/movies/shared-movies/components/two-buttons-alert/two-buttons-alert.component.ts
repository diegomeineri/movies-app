import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-two-buttons-alert',
  template: `
    <div class="tba__body modal-content">
      <div class="tba__body__title">
        <ion-label class="ux-font-text-lg">{{ this.title }} </ion-label>
      </div>
      <div class="tba__body__description">
        <ion-text class="ux-font-text-base">{{ this.description }} </ion-text>
      </div>
      <div class="tba__actions">
        <ion-button
          class="tba__actions__cancel"
          fill="clear"
          name="cancel"
          (click)="this.cancel()"
        >
          {{ this.cancelButton }}
        </ion-button>
        <ion-button class="tba__actions__confirm" fill="clear" name="confirm" (click)="this.confirm()">
          {{ this.confirmButton }}
        </ion-button>
      </div>
    </div>
  `,
  styleUrls: ['./two-buttons-alert.component.scss'],
})
export class TwoButtonsAlertComponent {
  title: string;
  description: string;
  cancelButton: string;
  confirmButton: string;

  constructor(private modalController: ModalController) {}

  confirm() {
    this.modalController.dismiss(null, 'confirm');
  }

  cancel() {
    this.modalController.dismiss(null, 'cancel');
  }
}
