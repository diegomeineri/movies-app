import { Component, Input, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Movie } from '../../interfaces/movie.interface';

@Component({
  selector: 'app-movie',
  template: `
    <div class="mov" [ngClass]="this.last ? 'last' : ''" (click)="this.goToMovieDetail()">
      <div class="mov__img">
        <img [src]="this.movie.img"/>
      </div>
      <div class="mov__content">
        <div class="mov__title_and_description">
          <ion-text class="mov__title_and_description__title">
            {{ this.movie.title }}
          </ion-text>
          <ion-text class="mov__title_and_description__description">
            {{ this.movie.description }}
          </ion-text>
        </div>
        <div class="mov__rate">
          <ion-text class="mov__rate__text">
            Rate:
          </ion-text>
          <ion-rating-stars
            [color]="'black'"
            [rating]="this.movie.rate"
            [filledColor]="'yellow'"
            [margin]="2"
            [size]="20"
            [disabled]="true"
          ></ion-rating-stars>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./movie.component.scss'],
})
export class MovieComponent implements OnInit {
  @Input() movie: Movie;
  @Input() last: boolean;
  constructor(private navController: NavController) {}

  ngOnInit() {}

  goToMovieDetail() {
    this.navController.navigateForward(['movies/detail/',this.movie.id])
  }

}
