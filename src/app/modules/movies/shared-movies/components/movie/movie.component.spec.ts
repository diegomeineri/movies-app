import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule, NavController } from '@ionic/angular';
import { FakeNavController } from 'src/app/testing/fakes/nav-controller.fake.spec';

import { MovieComponent } from './movie.component';

describe('MovieComponent', () => {
  let component: MovieComponent;
  let fixture: ComponentFixture<MovieComponent>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;
  const movie = {
    id: 1,
    title: 'Batman',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    rate: 1,
    img: 'assets/img/movies/batman.jpg',
    premiere: '10/03/2021',
  };
  beforeEach(waitForAsync(() => {
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    TestBed.configureTestingModule({
      declarations: [ MovieComponent ],
      imports: [IonicModule.forRoot()],
      providers: [
        { provide: NavController, useValue: navControllerSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieComponent);
    component = fixture.componentInstance;
    component.movie = movie;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render properly', () => {
    const imgEl = fixture.debugElement.query(By.css('div.mov__img img'));
    const titleEl = fixture.debugElement.query(
      By.css('ion-text.mov__title_and_description__title')
    );
    const descriptionEl = fixture.debugElement.query(
      By.css('ion-text.mov__title_and_description__description')
    );
    const rateEl = fixture.debugElement.query(
      By.css('ion-rating-stars')
    );

    expect(imgEl).toBeTruthy();
    expect(titleEl).toBeTruthy();
    expect(descriptionEl).toBeTruthy();
    expect(rateEl).toBeTruthy();
  });

  it('should navigate to detail when div is clicked', () => {
    const divEl = fixture.debugElement.query(By.css('div.mov__img img'));
    divEl.nativeElement.click();
    fixture.detectChanges();
    expect(navControllerSpy.navigateForward).toHaveBeenCalledOnceWith((['movies/detail/', movie.id]))
  });
});
