import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { IonRatingStarsModule } from 'ion-rating-stars';
import { MovieComponent } from './components/movie/movie.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { TwoButtonsAlertComponent } from './components/two-buttons-alert/two-buttons-alert.component';
import { UxDatetimeComponent } from './components/ux-datetime/ux-datetime.component';

@NgModule({
  declarations: [MovieComponent, SearchBarComponent, TwoButtonsAlertComponent, UxDatetimeComponent],
  imports: [CommonModule, IonicModule, ReactiveFormsModule, IonRatingStarsModule],
  exports: [MovieComponent, UxDatetimeComponent, SearchBarComponent, IonRatingStarsModule,  TwoButtonsAlertComponent, ReactiveFormsModule, CommonModule, IonicModule],
})
export class SharedMoviesModule {}
