import { NgModule } from '@angular/core';
import { MovieEditorPage } from './movie-editor.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedMoviesModule } from '../shared-movies/shared-movies.module';
const routes: Routes = [
  {
    path: '',
    component: MovieEditorPage,
  },
  {
    path: ':id',
    component: MovieEditorPage,
  },
];


@NgModule({
  imports: [
    SharedMoviesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [MovieEditorPage]
})
export class MovieEditorPageModule {}
