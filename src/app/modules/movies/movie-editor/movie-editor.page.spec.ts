import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { IonicModule, ModalController, NavController } from '@ionic/angular';
import { FakeActivatedRoute } from 'src/app/testing/fakes/activated-route.fake.spec';
import { FakeModalController } from 'src/app/testing/fakes/modal-controller.fake.spec';
import { FakeNavController } from 'src/app/testing/fakes/nav-controller.fake.spec';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';

import { MovieEditorPage } from './movie-editor.page';

describe('MovieEditorPage', () => {
  let component: MovieEditorPage;
  let fixture: ComponentFixture<MovieEditorPage>;
  let fakeActivatedRoute: FakeActivatedRoute;
  let activatedRouteSpy: jasmine.SpyObj<ActivatedRoute>;
  let moviesDataServiceSpy: jasmine.SpyObj<MoviesDataService>;
  let fakeModalController: FakeModalController;
  let modalControllerSpy: jasmine.SpyObj<ModalController>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;

  const movie = {
    id: 1,
    title: 'Batman',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    rate: 1,
    img: 'assets/img/movies/batman.jpg',
    premiere: '10/03/2021',
  };

  const editedMovie = {
    title: 'newTitle',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
    premiere: '10/03/2021',
    rate: 1,
    img: 'assets/img/movies/batman.jpg',
    id: 1,
  };

  beforeEach(waitForAsync(() => {
    moviesDataServiceSpy = jasmine.createSpyObj('MoviesDataService', {
      getMovie: [movie],
      indexOf: 1,
      editMovie: {},
    });
    fakeModalController = new FakeModalController(undefined, {});
    modalControllerSpy = fakeModalController.createSpy();
    fakeActivatedRoute = new FakeActivatedRoute({ id: '1' });
    activatedRouteSpy = fakeActivatedRoute.createSpy();
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    TestBed.configureTestingModule({
      declarations: [MovieEditorPage],
      imports: [IonicModule.forRoot(), ReactiveFormsModule],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteSpy },
        { provide: MoviesDataService, useValue: moviesDataServiceSpy },
        { provide: ModalController, useValue: modalControllerSpy },
        { provide: NavController, useValue: navControllerSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(MovieEditorPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set form data on init', () => {
    component.ngOnInit();
    fixture.detectChanges();
    expect(component.form.value.title).toEqual(movie.title);
    expect(component.form.value.description).toEqual(movie.description);
    expect(component.form.value.premiere).toEqual(movie.premiere);
    expect(component.form.value.rate).toEqual(movie.rate);
  });

  it('should open premiere modal when premiere input is clicked', () => {
    component.ngOnInit();
    fixture.detectChanges();
    const premiereInputEl = fixture.debugElement.query(
      By.css('ion-input.premiere')
    );
    premiereInputEl.nativeElement.click();
    expect(modalControllerSpy.create).toHaveBeenCalledTimes(1);
  });

  it('should patch premiere when user select premiere date on modal', async () => {
    fakeModalController.modifyReturns({}, { data: '25/02/1993' });
    component.ngOnInit();
    fixture.detectChanges();
    const premiereInputEl = fixture.debugElement.query(
      By.css('ion-input.premiere')
    );
    premiereInputEl.nativeElement.click();
    await fixture.whenRenderingDone();
    fixture.detectChanges();
    expect(modalControllerSpy.create).toHaveBeenCalledTimes(1);
    expect(component.form.value.premiere).toEqual('25/02/1993');
  });

  it('should edit movie and navigate when form is valid', async () => {
    component.ngOnInit();
    await fixture.whenRenderingDone();
    fixture.detectChanges();
    component.form.patchValue({ title: 'newTitle' });
    const editButtonEl = fixture.debugElement.query(
      By.css('div.me__button ion-button')
    );
    editButtonEl.nativeElement.click();
    await fixture.whenRenderingDone();
    fixture.detectChanges();
    expect(moviesDataServiceSpy.editMovie).toHaveBeenCalledOnceWith(
      1,
      editedMovie
    );
  });

  it('should disable Edit button if form is not valid', () => {
    component.form.patchValue({ title: '' });
    fixture.detectChanges();

    const editButtonEl = fixture.debugElement.query(
      By.css('div.me__button ion-button')
    );

    expect(editButtonEl.properties['disabled']).toBeTrue();
  });

  it('should patch rate when ion-rate-stars handle event', () => {
    fixture.debugElement
      .query(By.css('ion-rating-stars'))
      .triggerEventHandler('ratingChange', 3);
    fixture.detectChanges();
    expect(component.form.value.rate).toEqual(3);
  });
});
