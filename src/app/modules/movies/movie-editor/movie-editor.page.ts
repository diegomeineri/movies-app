import { Component, OnInit } from '@angular/core';
import {
  UntypedFormBuilder,
  UntypedFormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ModalController, NavController } from '@ionic/angular';
import { UxDatetimeComponent } from '../shared-movies/components/ux-datetime/ux-datetime.component';
import { Movie } from '../shared-movies/interfaces/movie.interface';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';

@Component({
  selector: 'app-movie-editor',
  template: `
    <ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button text="" mode="ios" defaultHref=""></ion-back-button>
        </ion-buttons>
        <ion-title class="me__header-title">{{ 'Edit' }}</ion-title>
      </ion-toolbar>
    </ion-header>
    <ion-content>
      <form [formGroup]="this.form">
        <div class="me__input">
          <ion-label class="me__input__label">Title</ion-label>
          <ion-item class="me__input__item">
            <ion-input formControlName="title"></ion-input>
          </ion-item>
        </div>
        <div class="me__input">
          <ion-label class="me__input__label">Description</ion-label>
          <ion-item class="me__input__item">
            <ion-textarea
              rows="6"
              maxlength="200"
              formControlName="description"
            ></ion-textarea>
          </ion-item>
        </div>
        <div class="me__input">
          <ion-label class="me__input__label">Premiere</ion-label>
          <ion-item class="me__input__item">
            <ion-input
              class="premiere"
              (click)="this.openPremiereModal()"
              formControlName="premiere"
            ></ion-input>
          </ion-item>
        </div>

        <div class="me__rate">
          <ion-rating-stars
            [color]="'white'"
            [rating]="this.movie.rate"
            [filledColor]="'yellow'"
            routerLinkActive=""
            [margin]="2"
            [size]="30"
            (ratingChange)="ratingChanged($event)"
          ></ion-rating-stars>
          <ion-text>Rate</ion-text>
        </div>
      </form>
    </ion-content>
    <ion-footer>
      <div class="me__button">
        <ion-button
          type="button"
          size="medium"
          [disabled]="!this.form.valid"
          (click)="this.editMovie()"
        >
          Save
        </ion-button>
      </div>
    </ion-footer>
  `,
  styleUrls: ['./movie-editor.page.scss'],
})
export class MovieEditorPage implements OnInit {
  isModalOpen = false;
  id : number;
  form: UntypedFormGroup = this.formBuilder.group({
    title: ['', [Validators.required]],
    description: ['', [Validators.required]],
    premiere: ['', [Validators.required]],
    rate: ['', [Validators.required]],
  });

  constructor(
    private route: ActivatedRoute,
    private formBuilder: UntypedFormBuilder,
    private navController: NavController,
    private moviesDataService: MoviesDataService,
    private modalController: ModalController
  ) {}

  movie: Movie;
  ngOnInit() {
    this.movieId();
    this.getMovie();
    this.patchForms();
  }

  ratingChanged(event: number) {
    this.form.patchValue({ rate: event });
  }

  async openPremiereModal() {
    if (!this.isModalOpen) {
      this.isModalOpen = true;
      const modal = await this.modalController.create({
        component: UxDatetimeComponent,
        cssClass: 'modal-date',
        backdropDismiss: false,
      });

      await modal.present();
      const data = await modal.onDidDismiss();
      if (data) {
        this.form.patchValue({ premiere: data.data });
      }
      this.isModalOpen = false;
    }
  }

  editMovie() {
    if (this.form.valid) {
      const index = this.moviesDataService.indexOf(this.movie);
      const editedMovie = Object.assign(this.form.value, {
        img: this.movie.img,
        id: this.movie.id,
      });
      this.moviesDataService.editMovie(index, editedMovie);
      this.navController.navigateBack('movies/home');
    }
  }

  getMovie() {
    this.movie = this.moviesDataService.getMovie(this.id)[0];
  }

  patchForms() {
    this.form.patchValue({ title: this.movie.title });
    this.form.patchValue({ description: this.movie.description });
    this.form.patchValue({ premiere: this.movie.premiere });
    this.form.patchValue({ rate: this.movie.rate });
  }

  movieId() {
    this.id = parseInt(this.route.snapshot.paramMap.get('id') as string);
  }
}
