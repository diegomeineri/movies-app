import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { IonicModule, NavController } from '@ionic/angular';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';
import { MoviesHomePage } from './movies-home.page';

describe('MoviesHomePage', () => {
  let component: MoviesHomePage;
  let fixture: ComponentFixture<MoviesHomePage>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let moviesDataServiceSpy: jasmine.SpyObj<MoviesDataService>;
  const movies = [
    {
      id: 1,
      title: 'Batman',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
      rate: 1,
      img: 'assets/img/movies/batman.jpg',
      premiere: '10/03/2021',
    },
    {
      id: 1,
      title: 'SpiderMan',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore',
      rate: 1,
      img: 'assets/img/movies/spiderman.jpg',
      premiere: '10/03/2021',
    },
  ];
  beforeEach(waitForAsync(() => {
    moviesDataServiceSpy = jasmine.createSpyObj('MoviesDataService', {
      getMovies: movies,
    });
    TestBed.configureTestingModule({
      declarations: [MoviesHomePage],
      imports: [IonicModule.forRoot()],
      providers: [
        { provide: NavController, useValue: navControllerSpy },
        { provide: MoviesDataService, useValue: moviesDataServiceSpy },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(MoviesHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render movies', () => {
    component.ionViewWillEnter();
    fixture.detectChanges();
    const [movieEl1, movieEl2] = fixture.debugElement.queryAll(
      By.css('app-movie')
    );
    expect(movieEl1).toBeTruthy();
    expect(movieEl2).toBeTruthy();
  });

  it('should reload movies when the input content is cleared', () => {
    component.ionViewWillEnter();
    fixture.detectChanges();
    fixture.debugElement
      .query(By.css('app-search-bar'))
      .triggerEventHandler('search', { target: { value: '' } });
    fixture.detectChanges();
    expect(component.movies.length).toEqual(2);
  });

  it('should filter movies when entering a text in ion-searchbar', () => {
    component.ionViewWillEnter();
    fixture.detectChanges();
    fixture.debugElement
      .query(By.css('app-search-bar'))
      .triggerEventHandler('search', { target: { value: 'Spid' } });
    fixture.detectChanges();
    expect(component.movies.length).toEqual(1);
  });
});
