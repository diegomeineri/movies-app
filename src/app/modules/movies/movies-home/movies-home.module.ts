import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedMoviesModule } from '../shared-movies/shared-movies.module';

import { MoviesHomePage } from './movies-home.page';

const routes: Routes = [
  {
    path: '',
    component: MoviesHomePage,
  },
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    SharedMoviesModule,
    RouterModule.forChild(routes),
  ],
  declarations: [MoviesHomePage],
})
export class MoviesHomePageModule {}
