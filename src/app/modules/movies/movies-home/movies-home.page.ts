import { Component, OnInit } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Movie } from '../shared-movies/interfaces/movie.interface';
import { MoviesDataService } from '../shared-movies/services/movies-data/movies-data.service';

@Component({
  selector: 'app-movies-home',
  template: `
    <ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-menu-button></ion-menu-button>
        </ion-buttons>
        <ion-title class="title">{{ 'Movies' }}</ion-title>
      </ion-toolbar>
    </ion-header>
    <app-search-bar
      (search)="this.handleChange($event)"
    ></app-search-bar>
    <ion-content class="mh">
      <div class="mh__body">
        <div *ngFor="let movie of this.movies; let last = last">
          <app-movie [movie]="movie" [last]="last"></app-movie>
        </div>
      </div>
      <ion-fab slot="fixed" vertical="bottom" horizontal="center">
        <ion-fab-button class="mh__new-button">
          <ion-icon name="add-sharp"></ion-icon>
        </ion-fab-button>
      </ion-fab>
    </ion-content>
  `,
  styleUrls: ['./movies-home.page.scss'],
})
export class MoviesHomePage implements OnInit {
  movies : Movie[];
  constructor(private moviesDataService: MoviesDataService, private menuController: MenuController) {}
  ngOnInit() {
    this.menuController.enable(true);
  }

  ionViewWillEnter() {
    this.getMovies();
  }
  
  getMovies(){
    this.movies = this.moviesDataService.getMovies();
  }
  
  handleChange(event: any) {
    const search = event.target.value.toLowerCase();
    if (search) {
      this.movies = this.movies.filter((movie: any) => {
        return movie.title.toLowerCase().indexOf(search) > -1;
      });
    } else if (search === '') {
      this.reloadMovies();
    }
  }

  reloadMovies() {
    this.movies = [];
    this.getMovies();
  }
}
