import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { UsersService } from '../shared-users/services/users/users.service';

@Component({
  selector: 'app-change-logo',
  template: `
    <ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref="/movies/home"
          ></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content>
      <div class="cl">
        <form class="cl__form" [formGroup]="this.form">
          <div *ngIf="!this.loading" class="cl__img">
            <img [src]="this.currentLogo"/>
          </div>
          <div *ngIf="this.loading" class="cl__loader">
              <ion-spinner name="circular"></ion-spinner>
              <ion-label>Loading image</ion-label>
          </div>
          <div class="log__input">
            <ion-item  class="cl__input__item">
              <label for="file-upload" slot="end">
                <ion-icon name="images" slot="end"></ion-icon>
              </label>
              <input
                id="file-upload"
                type="file"
                accept="image/*"
                placeholder="Select new logo"
                formControlName="image"
                (change)="this.configureImage($event)"
              />
            </ion-item>
            <div class="configured-label">
              <ion-label  *ngIf="this.configuredImage">Image: {{this.anImage.name}}...</ion-label>
            </div>
          </div>
          <div class="cl__button">
            <ion-button
              type="button"
              size="medium"
              [disabled]="!this.form.valid"
              (click)="this.changeLogo()"
            >
              Change Logo
            </ion-button>
          </div>
        </form>
      </div>
    </ion-content>
  `,
  styleUrls: ['./change-logo.page.scss'],
})
export class ChangeLogoPage implements OnInit {
  anImage;
  currentLogo: string;
  loading = false;
  configuredImage = false;
  form: UntypedFormGroup = this.formBuilder.group({
    image: ['', [Validators.required]],
  });
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService
  ) {}

  ngOnInit() {
    this.getCurrentLogo();
  }

  getCurrentLogo() {
    this.currentLogo = this.usersService.getLogoUrl();
    this.loading = false;
  }

  configureImage(event: any) {
    this.anImage = event?.target.files[0];
    this.configuredImage = true;
  }

  async changeLogo() {
    this.loading = true;
    await this.usersService.uploadImage(this.anImage, 'Movies', 'logo');
    this.getCurrentLogo();
  }
}
