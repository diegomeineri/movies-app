import { NgModule } from '@angular/core';
import { ChangeLogoPage } from './change-logo.page';
import { RouterModule, Routes } from '@angular/router';
import { SharedUsersModule } from '../shared-users/shared-users.module';
const routes: Routes = [
  {
    path: '',
    component: ChangeLogoPage,
  },

];
@NgModule({
  imports: [
    SharedUsersModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ChangeLogoPage]
})
export class ChangeLogoPageModule {}
