import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { UsersService } from '../shared-users/services/users/users.service';

import { ChangeLogoPage } from './change-logo.page';

describe('ChangeLogoPage', () => {
  let component: ChangeLogoPage;
  let fixture: ComponentFixture<ChangeLogoPage>;
  let usersServiceSpy: jasmine.SpyObj<UsersService>;
  beforeEach(waitForAsync(() => {
    usersServiceSpy = jasmine.createSpyObj('UsersService', {
      login: Promise.resolve(),
      getLogoUrl: 'test-logo'
    });
    TestBed.configureTestingModule({
      declarations: [ ChangeLogoPage ],
      imports: [IonicModule.forRoot()],
      providers:[{ provide: UsersService, useValue: usersServiceSpy },]
    }).compileComponents();

    fixture = TestBed.createComponent(ChangeLogoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
