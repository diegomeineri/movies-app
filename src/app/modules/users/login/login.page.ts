import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { MenuController, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { UsersService } from '../shared-users/services/users/users.service';

@Component({
  selector: 'app-login',
  template: `
    <ion-content>
      <div class="log">
        <form class="log__form" [formGroup]="this.form">
          <div class="log__img">
            <img
              [src]="this.currentLogo"
            />
          </div>
          <div class="log__input">
            <ion-item class="log__input__item username">
              <ion-input
                placeholder="Username"
                formControlName="user"
              ></ion-input>
            </ion-item>
          </div>
          <div class="log__input">
            <ion-item class="log__input__item">
              <ion-input
                placeholder="password"
                type="text"
                *ngIf="this.showPassword"
                formControlName="password"
              ></ion-input>
              <ion-input
                placeholder="Password"
                type="password"
                *ngIf="!this.showPassword"
                formControlName="password"
              ></ion-input>
              <button
                class="log__input__item__eyes"
                icon-only
                type="button"
                item-right
                (click)="this.togglePasswordMode()"
              >
                <ion-icon class="eye" *ngIf="this.showPassword" name="eye-outline"></ion-icon>
                <ion-icon  class="eye" *ngIf="!this.showPassword" name="eye-off-outline"></ion-icon>
              </button>
            </ion-item>
          </div>
          <div class="log__button">
            <ion-button
              type="button"
              size="medium"
              [disabled]="!this.form.valid"
              (click)="this.login()"
            >
              Login
            </ion-button>
            <ion-label
              class="log__button__create"
              (click)="this.createAccount()"
              >Create account</ion-label
            >
          </div>
        </form>
      </div>
    </ion-content>
  `,
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  showPassword = false;
  currentLogo: string;
  form: UntypedFormGroup = this.formBuilder.group({
    user: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required]],
  });
  constructor(
    private toastController: ToastController,
    private formBuilder: FormBuilder,
    private navController: NavController,
    private usersService: UsersService, 
    private menuController: MenuController,
    private storage: Storage
  ) {}

  ngOnInit() {}

  async ionViewWillEnter(){
    await this.storage.set('isLoggedIn', false);
    this.menuController.enable(false);
    this.setCurrentLogo();
  }

  setCurrentLogo(){
    this.currentLogo = this.usersService.getLogoUrl();
  }

  togglePasswordMode() {
    this.showPassword = this.showPassword === false ? true : false;
  }

   login() {
    this.usersService
      .login(this.form.value.user, this.form.value.password)
      .then(async (res) => {
        await this.storage.set('isLoggedIn', true);
        this.navController.navigateForward('/movies/home');
      })
      .catch((error) => {
        this.showErrorToast();
      });
  }

  async showErrorToast() {
    const toast = await this.toastController.create({
      color: 'danger',
      message: 'Incorrect Password or User',
      duration: 1500,
      position: 'top',
    });

    await toast.present();
  }

  createAccount() {
    this.navController.navigateForward('/users/register');
  }
}
