import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule, NavController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { FakeNavController } from 'src/app/testing/fakes/nav-controller.fake.spec';
import { UsersService } from '../shared-users/services/users/users.service';

import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;
  let usersServiceSpy: jasmine.SpyObj<UsersService>;
  let toastControllerSpy: jasmine.SpyObj<ToastController>;
  let storageServiceSpy: jasmine.SpyObj<Storage>;
  beforeEach(waitForAsync(() => {
    toastControllerSpy = jasmine.createSpyObj('ToastController', {
      create: Promise.resolve({ present: () => Promise.resolve() }),
    });
    usersServiceSpy = jasmine.createSpyObj('UsersService', {
      login: Promise.resolve(),
    });
    storageServiceSpy =  jasmine.createSpyObj('Storage', {
      set:Promise.resolve()
    });
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    TestBed.configureTestingModule({
      declarations: [LoginPage],
      imports: [IonicModule.forRoot(), ReactiveFormsModule],
      providers: [
        { provide: NavController, useValue: navControllerSpy },
        { provide: ToastController, useValue: toastControllerSpy },
        { provide: UsersService, useValue: usersServiceSpy },
        { provide: Storage, useValue: storageServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable login button if form is not valid', () => {
    component.form.patchValue({ email: '' });
    fixture.detectChanges();

    const editButtonEl = fixture.debugElement.query(
      By.css('div.log__button ion-button')
    );

    expect(editButtonEl.properties['disabled']).toBeTrue();
  });

  it('should login and navigate to home', async () => {
    component.form.patchValue({
      email: 'test@gmail.com',
      password: 'testPassword',
    });
    const logButtonEl = fixture.debugElement.query(
      By.css('div.log__button ion-button')
    );
    logButtonEl.nativeElement.click();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(navControllerSpy.navigateForward).toHaveBeenCalledOnceWith(
      '/movies/home'
    );
  });

  it('should present toast when login failed', async () => {
    usersServiceSpy.login.and.rejectWith('error');
    component.form.patchValue({ email: 'test@gmail.com', password: '453131' });
    const logButtonEl = fixture.debugElement.query(
      By.css('div.log__button ion-button')
    );
    logButtonEl.nativeElement.click();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(toastControllerSpy.create).toHaveBeenCalledTimes(1);
    expect(navControllerSpy.navigateForward).not.toHaveBeenCalled();
  });

  it('should navigate to register when create account button is clicked', async () => {
    const createButtonEl = fixture.debugElement.query(
      By.css('ion-label.log__button__create')
    );
    createButtonEl.nativeElement.click();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(navControllerSpy.navigateForward).toHaveBeenCalledOnceWith('/users/register');
  });
});
