import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Storage } from '@ionic/storage-angular';
import { BehaviorSubject } from 'rxjs';
import { UsersService } from './users.service';

const FirestoreStub = {
  collection: (name: string) => ({
    doc: (_id: string) => ({
      valueChanges: () => new BehaviorSubject({ foo: 'bar' }),
      set: (_d: any) => new Promise((resolve, _reject) => resolve('ok')),
    }),
  }),
};

describe('UsersService', () => {
  let service: UsersService;
  let storageServiceSpy: jasmine.SpyObj<Storage>;
  beforeEach(() => {
    storageServiceSpy =  jasmine.createSpyObj('Storage', {
      get:Promise.resolve()
    });
    TestBed.configureTestingModule({
      providers: [
        { provide: AngularFireAuth, useValue: FirestoreStub },
        { provide: AngularFireStorage, useValue: FirestoreStub },
        { provide: Storage, useValue: storageServiceSpy },
      ],
    });
    service = TestBed.inject(UsersService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
