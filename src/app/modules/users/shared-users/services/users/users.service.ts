import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireStorage } from '@angular/fire/compat/storage';
import { Storage } from '@ionic/storage-angular';
import { finalize } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  logoUrl = 'assets/img/initial/initial.png';
  constructor(
    private ngFireAuth: AngularFireAuth,
    private fireStorage: AngularFireStorage,
    private storage: Storage
  ) {}

  login(email: string, password: string) {
    return this.ngFireAuth.signInWithEmailAndPassword(email, password);
  }

  register(email: string, password: string) {
    return this.ngFireAuth.createUserWithEmailAndPassword(email, password);
  }

  logOut() {
    return this.ngFireAuth.signOut();
  }

  uploadImage(file: any, path: string, name: string): Promise<string> {
    return new Promise((resolve) => {
      const filePath = path + '/' + name;
      const ref = this.fireStorage.ref(filePath);
      const task = ref.put(file);
      task.snapshotChanges().pipe(finalize(() => {
        ref.getDownloadURL().subscribe(res => {
          this.logoUrl = res;
          resolve(this.logoUrl)
        });
      })).subscribe();
    });
  }

  getLogoUrl(){
    return this.logoUrl;
  }

  isLoggedIn() {
    return this.storage.get('isLoggedIn');
  }
}
