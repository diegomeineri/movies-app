import { Component, OnInit } from '@angular/core';
import { FormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { UsersService } from '../shared-users/services/users/users.service';

@Component({
  selector: 'app-register',
  template: `
    <ion-header>
      <ion-toolbar color="black">
        <ion-buttons slot="start">
          <ion-back-button
            text=""
            mode="ios"
            defaultHref="/users/login"
          ></ion-back-button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content>
      <div class="reg">
        <form class="reg__form" [formGroup]="this.form">
          <ion-text class="reg__title">New User</ion-text>
          <div class="reg__input">
            <ion-item class="reg__input__item username">
              <ion-input
                placeholder="Email"
                formControlName="email"
              ></ion-input>
            </ion-item>
          </div>
          <div class="reg__input">
            <ion-item class="reg__input__item">
              <ion-input
                placeholder="Password"
                type="text"
                *ngIf="this.showPassword"
                formControlName="password"
              ></ion-input>
              <ion-input
                placeholder="Password"
                type="password"
                *ngIf="!this.showPassword"
                formControlName="password"
              ></ion-input>
              <button
                class="reg__input__item__eyes"
                icon-only
                type="button"
                item-right
                (click)="this.togglePasswordMode()"
              >
                <ion-icon
                  class="eye"
                  *ngIf="this.showPassword"
                  name="eye-outline"
                ></ion-icon>
                <ion-icon
                  class="eye"
                  *ngIf="!this.showPassword"
                  name="eye-off-outline"
                ></ion-icon>
              </button>
            </ion-item>
          </div>
          <div class="reg__button">
            <ion-button
              type="button"
              size="medium"
              [disabled]="!this.form.valid"
              (click)="this.createAccount()"
            >
              Create
            </ion-button>
          </div>
        </form>
      </div>
    </ion-content>
  `,
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  showPassword = false;
  form: UntypedFormGroup = this.formBuilder.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });
  constructor(
    private formBuilder: FormBuilder,
    private usersService: UsersService,
    private toastController: ToastController,
    private navController: NavController
  ) {}

  ngOnInit() {}

  togglePasswordMode() {
    this.showPassword = this.showPassword === false ? true : false;
  }

  createAccount() {
    if (this.form.valid) {
      this.usersService
        .register(this.form.value.email, this.form.value.password)
        .then(() => {
          this.showSuccessToast();
          this.navController.navigateBack('/users/login');
        })
        .catch((error) => {
          this.showErrorToast();
        });
    }
  }

  async showErrorToast() {
    const toast = await this.toastController.create({
      color: 'danger',
      message: 'Something went wrong!. Try with other email',
      duration: 1500,
      position: 'top',
    });

    await toast.present();
  }

  async showSuccessToast() {
    const toast = await this.toastController.create({
      color: 'success',
      message: 'User successfully created',
      duration: 1500,
      position: 'top',
    });

    await toast.present();
  }
}
