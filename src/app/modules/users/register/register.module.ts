import { NgModule } from '@angular/core';
import { RegisterPage } from './register.page';
import { SharedUsersModule } from '../shared-users/shared-users.module';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: RegisterPage,
  },

];

@NgModule({
  imports: [
    SharedUsersModule,
    RouterModule.forChild(routes)
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {}
