import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { IonicModule, NavController, ToastController } from '@ionic/angular';
import { FakeNavController } from 'src/app/testing/fakes/nav-controller.fake.spec';
import { UsersService } from '../shared-users/services/users/users.service';

import { RegisterPage } from './register.page';

describe('RegisterPage', () => {
  let component: RegisterPage;
  let fixture: ComponentFixture<RegisterPage>;
  let navControllerSpy: jasmine.SpyObj<NavController>;
  let fakeNavController: FakeNavController;
  let usersServiceSpy: jasmine.SpyObj<UsersService>;
  let toastControllerSpy: jasmine.SpyObj<ToastController>;
  beforeEach(waitForAsync(() => {
    toastControllerSpy = jasmine.createSpyObj('ToastController', {
      create: Promise.resolve({ present: () => Promise.resolve() }),
    });
    usersServiceSpy = jasmine.createSpyObj('UsersService', {
      register: Promise.resolve(),
    });
    fakeNavController = new FakeNavController();
    navControllerSpy = fakeNavController.createSpy();
    TestBed.configureTestingModule({
      declarations: [ RegisterPage ],
      imports: [IonicModule.forRoot(), ReactiveFormsModule],
      providers: [
        { provide: NavController, useValue: navControllerSpy },
        { provide: ToastController, useValue: toastControllerSpy },
        { provide: UsersService, useValue: usersServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should disable create button if form is not valid', () => {
    component.form.patchValue({ email: '' });
    fixture.detectChanges();

    const editButtonEl = fixture.debugElement.query(
      By.css('div.reg__button ion-button')
    );

    expect(editButtonEl.properties['disabled']).toBeTrue();
  });

  it('should create user and navigate to login', async () => {
    component.form.patchValue({
      email: 'test@gmail.com',
      password: 'testPassword',
    });
    const logButtonEl = fixture.debugElement.query(
      By.css('div.reg__button ion-button')
    );
    logButtonEl.nativeElement.click();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(toastControllerSpy.create).toHaveBeenCalledTimes(1);
    expect(navControllerSpy.navigateBack).toHaveBeenCalledOnceWith(
      '/users/login'
    );
  });

  it('should show something wrong toast when error register', async () => {
    usersServiceSpy.register.and.rejectWith('error')
    component.form.patchValue({
      email: 'test@gmail.com',
      password: 'testPassword',
    });
    const logButtonEl = fixture.debugElement.query(
      By.css('div.reg__button ion-button')
    );
    logButtonEl.nativeElement.click();
    await fixture.whenStable();
    fixture.detectChanges();
    expect(toastControllerSpy.create).toHaveBeenCalledTimes(1);
  });

});
