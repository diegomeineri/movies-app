import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Observable } from 'rxjs';
import { UsersService } from 'src/app/modules/users/shared-users/services/users/users.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  login : boolean;
  constructor(private navController: NavController, private usersService: UsersService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {  
      this.isLogged().then((res)=>{
        if(!res){
          this.navController.navigateForward('users/login')
        }
      })
      return true;
  }

   async isLogged(){
     await this.usersService.isLoggedIn().then((res)=>{
      this.login = res;
    })
    return this.login;
  }

}
