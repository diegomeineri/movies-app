import { TestBed } from '@angular/core/testing';
import { UsersService } from 'src/app/modules/users/shared-users/services/users/users.service';
import { AuthGuard } from './auth.guard';


describe('AuthGuard', () => {
  let guard: AuthGuard;
  let usersServiceSpy: jasmine.SpyObj<UsersService>;
  beforeEach(() => {
    usersServiceSpy = jasmine.createSpyObj('UsersService', {
      isLoggedIn: Promise.resolve(true),
    });
    TestBed.configureTestingModule({
      providers:[{provide: UsersService, useValue: usersServiceSpy}],
    });
    guard = TestBed.inject(AuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
