import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { MoviesDataService } from './modules/movies/shared-movies/services/movies-data/movies-data.service';
import { UsersService } from './modules/users/shared-users/services/users/users.service';
@Component({
  selector: 'app-root',
  template:`
  <ion-app>
  <ion-split-pane contentId="main-content">
    <ion-menu contentId="main-content" type="overlay">
      <ion-content>
        <ion-list id="inbox-list">
          <ion-list-header>Menú</ion-list-header>
          <ion-menu-toggle auto-hide="false" *ngFor="let p of appPages; let i = index">
            <ion-item class="item" routerDirection="root" mode="ios" (click)="this.clickedItem(p)" lines="none" detail="false" routerLinkActive="selected">
              <ion-icon slot="start" [name]="p.icon" ></ion-icon>
              <ion-label>{{ p.title }}</ion-label>
            </ion-item>
          </ion-menu-toggle>
        </ion-list>
      </ion-content>
    </ion-menu>
    <ion-router-outlet id="main-content"></ion-router-outlet>
  </ion-split-pane>
</ion-app>`,
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit{
  public appPages = [
    { title: 'Change Logo', url: '/users/change-logo', icon: 'image' },
    { title: 'Log out', url: '/users/login', icon: 'log-out' },
  ];
  constructor(private storage : Storage, private moviesDataService: MoviesDataService, private navController: NavController, private usersService: UsersService) {}
  async ngOnInit() {
    await this.storage.create();
    this.initializeMoviesService()
  }

  async clickedItem(p : any){
    if(p.title === 'Log out') {
      await this.logOut();
    }else{
      this.navController.navigateForward(p.url)
    }
  }

  async logOut(){
    await this.storage.set('isLoggedIn', false);
    this.usersService.logOut().then(() => {
      this.navController.navigateRoot('/users/login');
    });
  }

  initializeMoviesService(){
    this.moviesDataService.init()
  }
}
