// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyAiMSXhRNJC0dhIkkVVQF30kiF9ADhnx40",
    authDomain: "movies-e5f0c.firebaseapp.com",
    projectId: "movies-e5f0c",
    storageBucket: "movies-e5f0c.appspot.com",
    messagingSenderId: "1031607820790",
    appId: "1:1031607820790:web:a77bed076e3dc5786067d7"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
